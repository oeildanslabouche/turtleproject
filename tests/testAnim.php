

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Tests Turtle, t'as vu ?</title>
</head>
<script src="pixi.min.js"></script>
<body>


    <script type="text/javascript">

    var app = new PIXI.Application();
    document.body.appendChild(app.view);

    var count = 0;

    // build a rope!
    var ropeLength = 50;

    var points = [];


    points.push(new PIXI.Point(0, 0));
    points.push(new PIXI.Point(0, 100));
    points.push(new PIXI.Point(100, 0));
    points.push(new PIXI.Point(100, 100));


    var strip = new PIXI.mesh.Mesh(PIXI.Texture.fromImage('images/tortue.png'), points);

    // strip.x = -459;

    var snakeContainer = new PIXI.Container();
    snakeContainer.x = 400;
    snakeContainer.y = 300;

    // snakeContainer.scale.set(800 / 1100);
    app.stage.addChild(snakeContainer);

    snakeContainer.addChild(strip);

    app.ticker.add(function() {

        count += 0.1;

        points[0].x += 10;
        points[1].x += 10;
        points[2].x += 10;
        points[3].x += 10;

        // // make the snake
        // for (var i = 0; i < points.length; i++) {
        //     points[i].y = Math.sin((i * 0.5) + count) * 100;
        //     points[i].x = i * ropeLength + Math.cos((i * 0.3) + count) * 100;
        // }
    });


    </script>
</body>
</html>
