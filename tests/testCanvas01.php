<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Tests Turtle, t'as vu ?</title>
</head>
<script src="pixi.min.js"></script>
<body>
    <label for="range">Opacity :</label>
    <input id="range" min="0" max="1" name="txt-range" step="0.01" type="range" value="" /><span id="myAlpha"></span><br>
    <label for="color">Couleur :</label>
    <input id="color" name="txt-color" type="color" value="#000000" /><span id="myColor"></span>
    <br />
    <script type="text/javascript">
    var  type = "WebGL"
    if(!PIXI.utils.isWebGLSupported()){
        type = "canvas"
    }

    var space = 50,
    stageWidth = 15,
    stageHeight = 11,
    stage = new PIXI.Container(),
    renderer = PIXI.autoDetectRenderer(space * stageWidth, space * stageHeight, {antialias: true, transparent: true, resolution: 1});
    document.body.appendChild(renderer.view);

    var resources = PIXI.loader.resources;

    //Use Pixi's built-in `loader` object to load an image
    PIXI.loader
    .add(["images/plage.png", "images/tortue.png"])
    .on("progress", loadProgressHandler)
    .load(setup);

    function loadProgressHandler(loader, resource) {
        console.log("loading: " + resource.url);
        //Display the percentage of files currently loaded
        console.log("progress: " + loader.progress + "%");
    }

    var plage, tortue, tortue2, state, grid, lines = [[], []];

    function play() {
        tortue.y -= tortue.vy;
        tortue.x -= tortue.vx;
    }

    function keyboard(keyCode) {
      var key = {};
      key.code = keyCode;
      key.isDown = false;
      key.isUp = true;
      key.press = undefined;
      key.release = undefined;
      //The `downHandler`
      key.downHandler = function(event) {
        if (event.keyCode === key.code) {
          if (key.isUp && key.press) key.press();
          key.isDown = true;
          key.isUp = false;
        }
        event.preventDefault();
      };

      //The `upHandler`
      key.upHandler = function(event) {
        if (event.keyCode === key.code) {
          if (key.isDown && key.release) key.release();
          key.isDown = false;
          key.isUp = true;
        }
        event.preventDefault();
      };

      //Attach event listeners
      window.addEventListener(
        "keydown", key.downHandler.bind(key), false
      );
      window.addEventListener(
        "keyup", key.upHandler.bind(key), false
      );
      return key;
    }

    //This `setup` function will run when the image has loaded
    function setup() {


        plage = new PIXI.Sprite(resources["images/plage.png"].texture);
        plage.width = space * stageWidth;
        plage.height = space * stageHeight;

        //Create the `tortue` sprite from the texture
        tortue = new PIXI.Sprite(resources["images/tortue.png"].texture);
        tortue.width = space;
        tortue.height = space;
        tortue.anchor.x = 0.5;
        tortue.anchor.y = 0.5;
        tortue.vx = 0;
        tortue.vy = 0;
        tortue.position.set(plage.width / 2, plage.height / 2);


        tortue2 = new PIXI.Sprite(resources["images/tortue.png"].texture);
        tortue2.width = space;
        tortue2.height = space;
        tortue2.anchor.x = 0.5;
        tortue2.anchor.y = 0.5;
        tortue2.vx = 0;
        tortue2.vy = 0;
        tortue2.position.set(plage.width / 2 + 2*space, plage.height / 2 + 2*space);

        stage.addChild(plage);
        //Grid
        grid = new PIXI.Container();
            //horizontal
        for (var i = 0; i < stageHeight; i++) {
            lines[0].push(new PIXI.Graphics());
            lines[0][i].lineStyle(1, 0xFFFFFF, 1);
            lines[0][i].moveTo(0, space*i);
            lines[0][i].lineTo(stageWidth * space, i * space);
            grid.addChild(lines[0][i]);
        }
            //vertical
        for (var i = 0; i < stageWidth; i++) {
            lines[1].push(new PIXI.Graphics());
            lines[1][i].lineStyle(1, 0xFFFFFF, 1);
            lines[1][i].moveTo(space*i, 0);
            lines[1][i].lineTo(i * space, stageHeight * space);
            grid.addChild(lines[1][i]);
        }

        grid.alpha= .5;
        stage.addChild(grid);
        stage.addChild(tortue2);
        stage.addChild(tortue);

        //Capture the keyboard arrow keys
          var left = keyboard(37),
              up = keyboard(38),
              right = keyboard(39),
              down = keyboard(40);

          //Left arrow key `press` method
          left.press = function() {

            //Change the turtle's velocity when the key is pressed
            tortue.x -= 50;
            //tortue.vy = 0;
          };

          //Left arrow key `release` method
          left.release = function() {

            //If the left arrow has been released, and the right arrow isn't down,
            //and the turtle isn't moving vertically:
            //Stop the turtle
            //if (!right.isDown && tortue.vy === 0) {
              tortue.vx = 0;
            //}
          };

          //Up
          up.press = function() {
            tortue.y -= 50;
            //tortue.vx = 0;
          };
          up.release = function() {
            //if (!down.isDown && tortue.vx === 0) {
              tortue.vy = 0;
            //}
          };

          //Right
          right.press = function() {
            tortue.x += 50;
            //tortue.vy = 0;
          };
          right.release = function() {
            //if (!left.isDown && tortue.vy === 0) {
              tortue.vx = 0;
            //}
          };

          //Down
          down.press = function() {
            tortue.y += 50;
            //tortue.vx = 0;
          };
          down.release = function() {
            //if (!up.isDown && tortue.vx === 0) {
              tortue.vy = 0;
            //}
          };

        state = play;

        gameLoop();
    }

    function hitTestRectangle(r1, r2) {

      //Define the variables we'll need to calculate
      var hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

      //hit will determine whether there's a collision
      hit = false;

      //Find the center points of each sprite
      r1.centerX = r1.x + r1.width / 2;
      r1.centerY = r1.y + r1.height / 2;
      r2.centerX = r2.x + r2.width / 2;
      r2.centerY = r2.y + r2.height / 2;

      //Find the half-widths and half-heights of each sprite
      r1.halfWidth = r1.width / 2;
      r1.halfHeight = r1.height / 2;
      r2.halfWidth = r2.width / 2;
      r2.halfHeight = r2.height / 2;

      //Calculate the distance vector between the sprites
      vx = r1.centerX - r2.centerX;
      vy = r1.centerY - r2.centerY;

      //Figure out the combined half-widths and half-heights
      combinedHalfWidths = r1.halfWidth + r2.halfWidth;
      combinedHalfHeights = r1.halfHeight + r2.halfHeight;

      //Check for a collision on the x axis
      if (Math.abs(vx) < combinedHalfWidths) {

        //A collision might be occuring. Check for a collision on the y axis
        if (Math.abs(vy) < combinedHalfHeights) {

          //There's definitely a collision happening
          hit = true;
        } else {

          //There's no collision on the y axis
          hit = false;
        }
      } else {

        //There's no collision on the x axis
        hit = false;
      }

      //`hit` will be either `true` or `false`
      return hit;
    };

    function gameLoop() {
        //Loop this function at 60 frames per second
        requestAnimationFrame(gameLoop);


        state();

        var alpha = document.getElementById('range').value;
        document.getElementById('myAlpha').innerHTML = alpha;
        var color = document.getElementById('color').value;
        document.getElementById('myColor').innerHTML = color.replace('#', '0x');

        for (var i = 0; i < stageHeight; i++) {
            lines[0][i].graphicsData[0]['lineColor']=color.replace('#', '0x');
            // lines[0][i].lineStyle(1, 0xFF3300, 1);
            // lines[0][i].alpha= alpha;
            lines[0][i].dirty++;
            lines[0][i].clearDirty++;

        }
            //vertical
        for (var i = 0; i < stageWidth; i++) {
            lines[1][i].graphicsData[0]['lineColor']=color.replace('#', '0x');
            // lines[1][i].alpha= alpha;
            // console.log(String.replace(color, '#', '0x'));
            lines[1][i].dirty++;
            lines[1][i].clearDirty++;
            //console.log(color);
            //console.log(String.replace(color, '#', '0x'));

        }
        grid.alpha = alpha;
        grid.dirty++;
        grid.clearDirty++;

        if (hitTestRectangle(tortue, tortue2)) {
            tortue.rotation+=0.1;
        }

  //Render the stage to see the animation
        renderer.render(stage);
    }



    </script>
</body>
</html>
